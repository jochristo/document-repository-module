package org.archiver.tools;

import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;

    /**
     * Contains utility methods for strings, objects, and file IO common operations.
     * @author ic
     */
    public class Utilities
    {
        
        /**
         * This method returns true if the collection is null or is empty.
         *
         * @param collection
         * @return true | false
         */
        public static boolean isEmpty(Collection<?> collection) {
            return collection == null || collection.isEmpty();
        }

        /**
         * This method returns true of the map is null or is empty.
         *
         * @param map
         * @return true | false
         */
        public static boolean isEmpty(Map<?, ?> map) {
            return map == null || map.isEmpty();
        }

        /**
         * This method returns true if the object is null.
         *
         * @param object
         * @return true | false
         */
        public static boolean isEmpty(Object object) {
            return (object == null);
        }

        /**
         * This method returns true if the input array is null or its length is
         * zero.
         *
         * @param array
         * @return true | false
         */
        public static boolean isEmpty(Object[] array) {
            return array == null || array.length == 0;
        }

        /**
         * This method returns true if the input string is null or its length is
         * zero.
         *
         * @param string
         * @return true | false
         */
        public static boolean isEmpty(String string) {
            return Strings.isNullOrEmpty(string);
        }

        /**
         * Returns the given input stream as string.
         *
         * @param is The input stream.
         * @return String The string representation of given input stream.
         */
        public static String getInputStreamAsString(InputStream is) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder stringBuider = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuider.append(line);
                }
                return stringBuider.toString();
            } catch (IOException ex) {
                Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }


        /**
         * Downloads file from given file path and writes to file using
         * BufferedInputStream. It returns the written bytes.
         *
         * @param filePath
         * @return byte
         */
        public static byte[] toByteArray(String filePath) {
            try {
                File file = new File(filePath);
                FileInputStream fis = new FileInputStream(file);
                byte[] bytes;
                try (BufferedInputStream inputStream = new BufferedInputStream(fis)) {
                    bytes = new byte[(int) file.length()];
                    inputStream.read(bytes);
                }
                return bytes;
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return null;
        }

        /**
         * Reads the bytes from given input stream, closes the stream and returns the bytes.
         * @param inputStream
         * @return
         * @throws IOException 
         */
        public static byte[] toBytes(InputStream inputStream) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, read);
            }
            baos.flush();
            baos.close();
            return baos.toByteArray();
        }

        /**
         * Deletes a file if it exists in given file path.
         *
         * @param filePath
         * @return
         * @throws IOException
         */
        public static boolean deleteFile(String filePath) throws IOException {
            File file = new File(filePath);
            boolean exists = (file.exists());
            if (exists) {
                file = new File(filePath);
                return java.nio.file.Files.deleteIfExists(file.toPath());
            }
            return false;
        }

        public static boolean deletePath(String dirPath) throws IOException {
            // Use NIO to remove all files and parent directory denoted by given directory path
            Path path = Paths.get(dirPath);
            Files.walkFileTree(path, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });

            return true;
        }

        public static boolean deletePathInclNonEmpty(String dirPath) throws IOException {
            // Use NIO to remove all files and parent directory denoted by given directory path
            Path path = Paths.get(dirPath);
            Files.walkFileTree(path, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    try {
                        if (deleteDirectory(file.toFile())) {
                            Files.delete(file);
                        }
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    try {
                        if (deleteDirectory(dir.toFile())) {
                            Files.delete(dir);
                        }
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });

            return true;
        }

        public static boolean deleteDirectory(File directoryToBeDeleted) throws URISyntaxException, IOException {
            File[] allContents = directoryToBeDeleted.listFiles();
            if (allContents != null) {
                for (File file : allContents) {
                    Files.delete(Paths.get(new URI(file.getAbsolutePath())));
                }
            }
            return directoryToBeDeleted.delete();
        }

        /**
         * Extracts first three characters from given nickname path and returns
         * a string as a directory tree path , i.e. a\b\c
         *
         * @param nicknamePath
         * @return String
         */
        public static String getPath(String nicknamePath) {
            if (!nicknamePath.isEmpty() && nicknamePath.length() >= 3) {
                String path = "";
                for (int i = 0; i < 3; i++) {
                    char c = nicknamePath.charAt(i); // check for . character and replace with _
                    char replacement = '_';
                    if (c == '.') {
                        c = replacement;
                    }
                    path += c;
                    if (path.length() < 5) {
                        path += "\\";
                    }
                }

                return path;
            }
            return null;
        }

        /**
         * Extracts and returns the first three characters from given string.
         *
         * @param value
         * @return String
         */
        public static String firstThreeChars(String value) {
            if (!value.isEmpty() && value.length() >= 3) {
                String extracted = "";
                for (int i = 0; i < 3; i++) {
                    char item = value.charAt(i);
                    if (value.charAt(i) == '.') {
                        item = '_';
                    }
                    extracted += item;
                }

                return extracted;
            }
            return null;
        }

        /**
         * Extracts and returns the first three characters from given string.
         *
         * @param value
         * @return String
         */
        public static String createPathFromName(String value) {
            if (!value.isEmpty() && value.length() >= 3) {
                String extracted = File.separator;
                for (int i = 0; i < 3; i++) {
                    char item = value.charAt(i);
                    if (value.charAt(i) == '.') {
                        item = '_';
                    }
                    extracted += item + File.separator;
                }

                return extracted;
            }
            return null;
        }

        public static String toString(long value) {
            return String.valueOf(value);
        }

        public static String toString(int value) {
            return String.valueOf(value);
        }

        public static String toString(char value) {
            return String.valueOf(value);
        }

        public static long toLong(String value) {
            return Long.parseLong(value);
        }

        public static long toInt(String value) {
            return Integer.parseInt(value);
        }

        public static boolean isEqual(String a, String b) {
            return (a.compareTo(b) == 0);
        }

        public static boolean isEqual(String a, long b) {
            return (a.compareTo(String.valueOf(b)) == 0);
        }

        public static boolean isEqual(String a, int b) {
            return (a.compareTo(String.valueOf(b)) == 0);
        }

        /**
         * Generates random alphanumeric filename appended to given filename.
         *
         * @param filename
         * @return
         */
        public static String randomFilename(String filename) {
            if (filename.length() >= 3) {
                char first = filename.charAt(0);
                char second = filename.charAt(1);
                char third = filename.charAt(2);
                String random = "";
                if (!isEmpty(first) && first == '.') {
                    first = '_';
                }
                if (!isEmpty(second) && second == '.') {
                    second = '_';
                }
                if (!isEmpty(third) && third == '.') {
                    third = '_';
                }
                char[] chars = {Character.toLowerCase(first), Character.toLowerCase(second), Character.toLowerCase(third)};
                random = new String(chars);
                return random.concat(RandomStringUtils.randomAlphanumeric(5));
            }
            return RandomStringUtils.randomAlphanumeric(8);
        }

        public static long roundUp(long dividend, long divisor) {
            return (dividend + divisor - 1) / divisor;
        }

        public static double roundHalfUp(double value, int places) {
            if (places < 0) {
                throw new IllegalArgumentException();
            }

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }

        /**
         * Writes given input steam to byte array.
         *
         * @param inputStream
         * @return
         */
        public static byte[] toByteArray(InputStream inputStream) {
            byte[] bytes = null;
            try {
                bytes = ByteStreams.toByteArray(inputStream);
            } catch (IOException ex) {
                Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);

            }
            return bytes;
        }

        /**
         * Reads given byte array and creates a ByteArrayInputStream object from
         * it.
         *
         * @param data
         * @return
         */
        public static InputStream toByteArrayInputStream(byte[] data) {
            InputStream is = new ByteArrayInputStream(data);
            return is;
        }

        public static byte[] toOutputStream(InputStream inputStream, String outputFile) throws FileNotFoundException, IOException {
            OutputStream outputStream;
            outputStream = new FileOutputStream(outputFile);
            // Use an 8K data buffer
            byte[] buffer = new byte[8192];
            int count;
            while ((count = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
            return buffer;
        }

        /**
         * returns the extension from the original filename.
         *
         * @param fileName
         * @return
         */
        public String parseExtension(final String fileName) {
            String[] split = fileName.split("\\.");
            if (split.length > 1) {
                return split[split.length - 1];
            }
            return null;
        }

        /**
         * returns the extension from the original filename.
         *
         * @param fileName
         * @return
         */
        public static String parseFilename(final String fileName) {
            String[] split = fileName.split("\\.");
            if (split.length > 1) {
                return split[0];
            }
            return null;
        }

        public static String sanitizeRepositoryName(String repository) {
            String replaced = repository.replaceAll("[^A-Za-z0-9-_.]", "");
            return replaced;
        }

        public static void write(byte[] data, String fullFilePath) throws FileNotFoundException, IOException {
            InputStream is = new ByteArrayInputStream(data);
            IOUtils.copy(is, new FileOutputStream(fullFilePath));
        }

        public static void write(byte[] data, String fullFilePath, boolean isAppended) throws FileNotFoundException, IOException {
            InputStream is = new ByteArrayInputStream(data);
            FileOutputStream fos = new FileOutputStream(fullFilePath, isAppended);
            IOUtils.copy(is, fos);
            // release
            fos.flush();
            is.close();
            fos.close();
        }

        public static void write(byte[] data, OutputStream outputStream) throws FileNotFoundException, IOException {
            InputStream is = new ByteArrayInputStream(data);
            IOUtils.copy(is, outputStream);
            // release
            outputStream.flush();
            is.close();
            outputStream.close();
        }

        public static boolean existsFile(String filePath) {
            File file = new File(filePath);
            return file.exists();
        }

        public static int randomPin(int count) throws NoSuchAlgorithmException {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            int value = ThreadLocalRandom.current().nextInt(1000, 9999);
            return random.nextInt(count);
        }

        public static int randomPin() {
            return ThreadLocalRandom.current().nextInt(1000, 9999);
        }

        public static int currentEpochPlus(int seconds) {
            LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(seconds);
            return (int) localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond();
        }
    
        
    
}
