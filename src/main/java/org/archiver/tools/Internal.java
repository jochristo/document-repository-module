package org.archiver.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.archiver.api.error.ApiErrorCode;

/**
 * Internal API utilities.
 * @author ic
 */
public class Internal
{
    
    /**
     * Gets a list of API error codes and their description.
     * @return 
     */
    public static List<ApplicationErrorCode> getApiErrorCodes()
    {
        List<ApplicationErrorCode> data = new ArrayList();
        ApiErrorCode[] apiErrorCodes = ApiErrorCode.values();
        List<ApiErrorCode> list = Arrays.asList(apiErrorCodes);
        if(list.isEmpty() == false){
            list.stream().forEachOrdered((item)->
            {
                ApplicationErrorCode code = new ApplicationErrorCode();
                code.setStatus(item.getHttpStatus().name());
                code.setValue(item.getHttpStatus().value());
                code.setCode(item.getAppCode());                
                code.setTitle(item.getTitle());                
                data.add(code);
            });
        }
        return data;
    }  
     
}
