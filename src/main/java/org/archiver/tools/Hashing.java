package org.archiver.tools;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.UUID;
import org.slf4j.LoggerFactory;

/**
 * Provides PBKDF2 with HMAC SHA1/SHA256 encryption.
 * @author ic
 */
public class Hashing
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Hashing.class);    
   
    /**
     * Creates a directory path of depth equal to givel dirDepth variable value and and format:
     * Extracts first two characters from string to form the parent directory followed by the next two characters to
     *  form the second level directory. E.g. filename: johndoe.doe, dirDepth=2 -> returns: \jo\hn\ or /jo/hn/
     * @param depth
     * @param filename
     * @return
     */    
    public static  String createDirectoryPathString(String filename, int depth){
      if(depth == 0) depth = 2;
      StringBuilder sb = new StringBuilder();      
      sb.append(File.separator);
      for(int x=0; x < depth; x++)
      {                             
          int i = depth*x;
          sb.append(filename.substring(i, i+depth));
          sb.append(File.separator);          
      }      
      return sb.toString();
    }                   
       
    public static String uuidToBase64(byte[] bytes) throws UnsupportedEncodingException{        
        
        UUID uuid = UUID.randomUUID();
        String base64 = Base64.getEncoder().encodeToString(uuid.toString().getBytes("utf-8"));
        return base64;
    }
    
    public static String uuidToBase64(String value) throws UnsupportedEncodingException{        
        
        UUID uuid = UUID.fromString(value);
        String base64 = Base64.getEncoder().encodeToString(uuid.toString().getBytes("utf-8"));
        return base64;
    }    
    
}
