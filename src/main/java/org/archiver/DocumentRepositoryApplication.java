package org.archiver;

import org.archiver.api.configuration.ArchiveStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties(ArchiveStorageProperties.class)
public class DocumentRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentRepositoryApplication.class, args);                
	}
}
