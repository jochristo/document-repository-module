package org.archiver.api.advisor;

import java.util.List;
import org.archiver.api.exception.BadChecksumException;
import org.archiver.api.exception.DataNotFoundException;
import org.archiver.api.exception.DocumentIdNotFoundException;
import org.archiver.api.exception.DocumentStorageException;
import org.archiver.api.exception.FieldConstraintValidationException;
import org.archiver.api.exception.FieldValidationException;
import org.archiver.api.exception.MissingParameterException;
import org.archiver.api.exception.RestApiException;
import org.archiver.api.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller exception advisory handler.
 * @author ic
 */
@ControllerAdvice(annotations = {RestController.class})
public class RestApiExceptionControllerAdviceImpl implements IRestApiExceptionControllerAdvice
{
    private static final Logger logger = LoggerFactory.getLogger(RestApiExceptionControllerAdviceImpl.class);          
     
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldConstraintValidationException.class})
    @ResponseBody    
    public List<ErrorResponse> handleFieldConstraintValidationException(FieldConstraintValidationException ex)
    {
        logger.error("FieldConstraintValidationException occured: ", ex);        
        return ex.getErrors();
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldValidationException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleFieldValidationException(FieldValidationException ex)
    {
        logger.error("FieldValidationException occured: ", ex);        
        return ex.getErrorResponse();
    }              
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DocumentIdNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleDocumentIdNotFoundException(DocumentIdNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse er = new ErrorResponse();        
        er.setError(ex.getMessage());         
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(er);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MissingParameterException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleMissingParameterException(MissingParameterException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ex.getErrorResponse();
    }  
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleDataNotFoundException(DataNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }    
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadChecksumException.class)
    @ResponseBody 
    @Override
    public ResponseEntity handleBadChecksumException(BadChecksumException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);       
        ErrorResponse er = new ErrorResponse();
        er.setError(ex.getMessage());        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(er);
    }    

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DocumentStorageException.class)
    @ResponseBody 
    @Override
    public ResponseEntity handleDocumentStorageException(DocumentStorageException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse er = new ErrorResponse();
        er.setError(ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(er);
    }   
    
    @Override
    public ResponseEntity handleLockedException(RestApiException ex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseEntity handleUnauthorizedException(RestApiException ex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseEntity handleUnprocessableEntityException(RestApiException ex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
