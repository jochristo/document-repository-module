package org.archiver.api.advisor;

import org.archiver.api.exception.BadChecksumException;
import org.archiver.api.exception.DataNotFoundException;
import org.archiver.api.exception.DocumentIdNotFoundException;
import org.archiver.api.exception.DocumentStorageException;
import org.archiver.api.exception.FieldValidationException;
import org.archiver.api.exception.MissingParameterException;
import org.archiver.api.exception.RestApiException;
import org.archiver.api.model.ErrorResponse;
import org.springframework.http.ResponseEntity;

/**
 * Defines the exception handling methods and its return type of the RESTful API.
 * @author ic
 */
public interface IRestApiExceptionControllerAdvice
{
    public ErrorResponse handleFieldValidationException(FieldValidationException ex);            
        
    public ResponseEntity handleLockedException(RestApiException ex);         
    
    public ResponseEntity handleUnauthorizedException(RestApiException ex);    
    
    public ResponseEntity handleUnprocessableEntityException(RestApiException ex);       
    
    public ResponseEntity handleDocumentIdNotFoundException(DocumentIdNotFoundException ex);        
        
    public ErrorResponse handleMissingParameterException(MissingParameterException ex);    
        
    public ResponseEntity handleDataNotFoundException(DataNotFoundException ex);
    
    public ResponseEntity handleBadChecksumException(BadChecksumException ex);
    
    public ResponseEntity handleDocumentStorageException(DocumentStorageException ex);
    
}
