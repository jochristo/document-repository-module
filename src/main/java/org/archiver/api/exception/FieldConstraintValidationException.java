package org.archiver.api.exception;

import java.util.List;
import org.archiver.api.model.ErrorResponse;


/**
 *
 * @author ic
 */
public class FieldConstraintValidationException extends RuntimeException
{
    private final List<ErrorResponse> errors;    
        
    public FieldConstraintValidationException(List<ErrorResponse> errors) {
        
        super("Field errors occured");                              
        this.errors = errors;        
    }    
    
    public List<ErrorResponse> getErrors() {
        return errors;
    }

    public FieldConstraintValidationException(List<ErrorResponse> errors, String message) {
        super(message);
        this.errors = errors;
    }
    
    
    
    
}
