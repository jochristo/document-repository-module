package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;
import org.archiver.api.model.ErrorResponse;



/**
 *
 * @author ic
 */
public class MissingParameterException extends RestApiException
{   
    private String parameter;
    protected ErrorResponse errorResponse;
    
    public MissingParameterException(String parameter, String details) {
        super(ApiErrorCode.MISSING_REQUEST_PARAMETER, details);
        this.parameter = parameter;
        this.errorResponse = new ErrorResponse();
        errorResponse.setError(details);
        errorResponse.setParameter(parameter);
    }

    public String getParameter() {
        return parameter;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }
    
    
    
    
}
