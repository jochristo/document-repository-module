package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;



/**
 *
 * @author ic
 */
public class DocumentIdNotFoundException extends RestApiException
{    
    public DocumentIdNotFoundException(String details) {
        super(ApiErrorCode.DOCUMENT_ID_NOT_FOUND,details);
    }
    
}
