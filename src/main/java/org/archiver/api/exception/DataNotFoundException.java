package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author ic
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DataNotFoundException extends RestApiException
{
    
    public DataNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }

    
}
