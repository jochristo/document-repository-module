package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class DocumentNotAvailableException extends RestApiException
{    
    public DocumentNotAvailableException(String details) {
        super(ApiErrorCode.DOCUMENT_NOT_AVAILABLE,details);
    }
    
}
