package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;
import org.archiver.api.model.ErrorResponse;



/**
 *
 * @author ic
 */
public class FieldValidationException extends RuntimeException
{
    protected ApiErrorCode apiErrorCode;
    protected ErrorResponse errorResponse;
    
    public FieldValidationException(String details) {
        super(details);
    }
    
    public FieldValidationException(ErrorResponse errorResponse){
        super(errorResponse.getError());
        this.apiErrorCode = ApiErrorCode.INVALID_FORMAT;
        this.errorResponse = errorResponse;
    }

    public ApiErrorCode getApiErrorCode() {
        return apiErrorCode;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }
    
    
    
}
