package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class BadChecksumException extends RestApiException
{    
    public BadChecksumException(String details) {
        super(ApiErrorCode.BAD_FILE_CHECKSUM,details);
    }
    
}
