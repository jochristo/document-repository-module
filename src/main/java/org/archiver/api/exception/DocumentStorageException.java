package org.archiver.api.exception;

import org.archiver.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class DocumentStorageException extends RestApiException {

    public DocumentStorageException(String details) {        
        super(ApiErrorCode.FILE_ERROR,details);
    }

    
}
