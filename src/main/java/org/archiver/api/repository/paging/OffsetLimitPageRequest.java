package org.archiver.api.repository.paging;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Provides limit, offset, sort, and paging capability.
 * @author ic
 */
public class OffsetLimitPageRequest extends PageRequest
{
    private final int limit;
    private final long offset;   
    private final Sort sort;
    
    public OffsetLimitPageRequest(int limit, long offset, Sort sort,int page, int size) {
        super(page, size);
        this.limit = limit;
        this.offset = offset;
        this.sort = sort;
    }
            
    @Override
    public int getPageNumber() {
        return 0;
    }
    @Override
    public int getPageSize() {
        return limit;
    }
    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }
    
    

}
