package org.archiver.api.repository;

import org.archiver.api.domain.ArchiveDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface ArchiveDetailsRepository extends MongoRepository<ArchiveDetails, String>
{
    @Query(value = "{ 'archive.id' : ?0 }")
    public ArchiveDetails findByArchiveId(String archiveId);
}
