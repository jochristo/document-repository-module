package org.archiver.api.repository;

import org.archiver.api.domain.Archive;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface ArchiveRepository extends MongoRepository<Archive, String>
{
    
}
