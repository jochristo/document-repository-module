package org.archiver.api.repository;

import java.util.List;
import org.archiver.api.domain.FileDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface FileDocumentRepository extends MongoRepository<FileDocument, String>
{
    @Query(value = "{ 'status' : 'ACTIVE' }")
    public List<FileDocument> findActive();
    
    @Query(value = "{ 'status' : 'INACTIVE' }")
    public List<FileDocument> findInActive();    
    
    @Query(value = "{ 'id' : ?0 , 'status' : 'ACTIVE' }")
    public List<FileDocument> findActive(String id);    
    
    @Query(value = "{ 'filename' : ?0 , 'status' : 'ACTIVE' }")
    public List<FileDocument> findActiveByFilename(String filename);         
    
}
