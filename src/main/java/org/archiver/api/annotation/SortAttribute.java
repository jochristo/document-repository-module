package org.archiver.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.archiver.api.domain.enumeration.DocumentSortField;
import org.archiver.api.validation.SortAttributeValidator;

/**
 *
 * @author ic
 */
@Documented
@Constraint(validatedBy = SortAttributeValidator.class)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SortAttribute
{
    String message() default "field does not exist";         
        
    DocumentSortField[] fields() default {DocumentSortField.CREATED_AT, DocumentSortField.FILENAME, DocumentSortField.STATUS};
     
    Class<?>[] groups() default {};
     
    Class<? extends Payload>[] payload() default {};     
}
