package org.archiver.api.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.codec.digest.DigestUtils;
import org.archiver.api.domain.Archive;
import org.archiver.api.domain.ArchiveDetails;
import org.archiver.api.domain.FileDocument;
import org.archiver.api.exception.BadChecksumException;
import org.archiver.api.exception.DocumentIdNotFoundException;
import org.archiver.api.exception.DocumentStorageException;
import org.archiver.api.model.DocumentModel;
import org.archiver.api.model.bind.DocumentUploadParameter;
import org.archiver.api.repository.ArchiveDetailsRepository;
import org.archiver.api.repository.ArchiveRepository;
import org.archiver.api.repository.FileDocumentRepository;
import org.archiver.api.service.IDocumentHandler;
import org.archiver.api.service.IDocumentStorage;
import org.archiver.tools.Hashing;
import org.archiver.tools.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Implements document handler methods to create, store, and serve documents.
 * @author ic
 */
@Service
public class DocumentHandlerImpl implements IDocumentHandler
{
    @Autowired private ArchiveRepository archiveRepository;    
    @Autowired private ArchiveDetailsRepository archiveDetailsRepository;      
    @Autowired private IDocumentStorage documentStorage;
    @Autowired private FileDocumentRepository repository;
    
    @Override
    @Deprecated
    public DocumentModel create(DocumentUploadParameter parameter)
    {        
        Archive archive = new Archive();        
        String filename = parameter.getFile().getOriginalFilename();        
        filename = Utilities.sanitizeRepositoryName(filename.toLowerCase());
        String path = Hashing.createDirectoryPathString(filename, 3);
        byte[] bytes;
        try
        {
            bytes = Utilities.toByteArray(parameter.getFile().getInputStream());
        } catch (IOException ex) {
            throw new DocumentStorageException("error reading bytes from document: " +parameter.getFile().getOriginalFilename());
        }
        String storageHash = DigestUtils.sha256Hex(bytes);
        
        archive.setFilename(filename); // served filename
        archive.setIntegrityHash(parameter.getIntegrity());        
        Archive arc = archiveRepository.save(archive);
                
        ArchiveDetails details = new ArchiveDetails();
        details.setArchive(arc);
        details.setPath(path);
        details.setRepository(DigestUtils.sha1Hex(UUID.randomUUID().toString())); //stored filename
        details.setStorageHash(storageHash);
        ArchiveDetails archiveDetails = archiveDetailsRepository.save(details);
        
        if(arc!=null && archiveDetails!= null)
        {
            documentStorage.store(parameter.getFile(), path, archiveDetails.getRepository());           
            DocumentModel dm = new DocumentModel();
            dm.setId(arc.getId());
            return dm;
        }
        return null;
    }
    
    @Override
    @Deprecated
    public Archive getArchive(String id)
    {
        Optional<Archive> optional = archiveRepository.findById(id);
        if(optional.isPresent() == false){
            throw new DocumentIdNotFoundException("document id not found: " + id);
        }        
        Archive archive = optional.get();

        return archive;
    }
    
    @Override
    @Deprecated
    public Resource getFile(String id)
    {
        Optional<Archive> optional = archiveRepository.findById(id);
        if(optional.isPresent() == false){
            throw new DocumentIdNotFoundException("document id not found: " + id);
        }                
        ArchiveDetails details = archiveDetailsRepository.findByArchiveId(id);
        if(details != null)
        {
            StringBuilder sb = new StringBuilder(details.getPath());
            sb.append(File.separator);
            Path fullPath = Paths.get(sb.toString());
            Resource res = documentStorage.get(fullPath.toString(), details.getRepository());            
            // check storage checksum
            String storageHash = details.getStorageHash();
            try
            {
                byte[] bytes = Utilities.toByteArray(res.getInputStream());
                String checksum = DigestUtils.sha256Hex(bytes);
                if(checksum.equals(storageHash) == false){
                    throw new BadChecksumException("file content is corrupted or tampered with: " + res.getFilename());
                }                
            }
            catch (IOException ex){
                throw new DocumentStorageException("error reading bytes from document: " + optional.get().getFilename());
            }            

            return res;
        }
        
        return null;
    }

    @Override
    @Deprecated
    public DocumentModel createDocument(DocumentUploadParameter parameter)
    {                       
        String filename = parameter.getFile().getOriginalFilename();        
        filename = Utilities.sanitizeRepositoryName(filename.toLowerCase());
        String path = Hashing.createDirectoryPathString(filename, 3);
        byte[] bytes;
        try
        {
            bytes = Utilities.toByteArray(parameter.getFile().getInputStream());
        } catch (IOException ex) {
            throw new DocumentStorageException("error reading bytes from document: " +parameter.getFile().getOriginalFilename());
        }
        String storageHash = DigestUtils.sha256Hex(bytes);
        
        // create new file document
        FileDocument document = new FileDocument();
        document.setFilename(filename);
        document.setChecksum(parameter.getIntegrity());
        document.setPath(path);
        document.setStoredName(DigestUtils.sha1Hex(UUID.randomUUID().toString())); //stored filename
        document.setHash(storageHash);
        
        // store and serrve
        FileDocument stored = repository.save(document);        
        
        if(stored != null){
            documentStorage.store(parameter.getFile(), path, document.getStoredName());           
            DocumentModel dm = new DocumentModel();
            dm.setId(document.getId());
            return dm;         
        }
        return null;
    }

    @Override
    @Deprecated
    public Resource getDocumentFile(String id)
    {
        Optional<Archive> optional = archiveRepository.findById(id);
        Optional<FileDocument> optionalDocument = repository.findById(id);
        if(optionalDocument.isPresent() == false){
            throw new DocumentIdNotFoundException("document id not found: " + id);
        }         
        
        // get document
        FileDocument document = optionalDocument.get();
        StringBuilder sb = new StringBuilder(document.getPath());
        sb.append(File.separator);
        Path fullPath = Paths.get(sb.toString());
        Resource res = documentStorage.get(fullPath.toString(), document.getStoredName());
        
        // check storage checksum
        String storageHash = document.getHash();
        try {
            byte[] bytes = Utilities.toByteArray(res.getInputStream());
            String checksum = DigestUtils.sha256Hex(bytes);
            if (checksum.equals(storageHash) == false) {
                throw new BadChecksumException("file content is corrupted or tampered with: " + res.getFilename());
            }
        } catch (IOException ex) {
            throw new DocumentStorageException("error reading bytes from document: " + optional.get().getFilename());
        }

        return res;
    }

    /**
     * Gets all FilDocument instances persistent in underlying data store.
     * @return 
     */
    @Override
    public List<FileDocument> getAllDocuments() {
        return repository.findAll();
    }

    /**
     * Gets all FilDocument instances persistent in underlying data store with given page request.
     * @param pageRequest
     * @return 
     */    
    @Override
    @Deprecated
    public List<FileDocument> getAllDocuments(PageRequest pageRequest) {
        return repository.findAll(pageRequest).getContent();
    }

    
    /**
     * Gets all FilDocument instances persistent in underlying data store with given pageable object.
     * Support for page size, offset, and sort.
     * @param pageable
     * @return 
     */
    @Override
    public List<FileDocument> getAllDocuments(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }
        
    /**
     * Gets the FileDocument instance from underlying data store with given document id.
     * @param id
     * @return 
     */
    @Override
    public FileDocument getDocument(String id) {
        Optional<FileDocument> optional = repository.findById(id);
        if(optional.isPresent() == false){
            throw new DocumentIdNotFoundException("document id not found: " + id);
        }        
        FileDocument document = optional.get();

        return document;
    }    
    
    /**
     * Creates and stores in file system given document properties and serves a DocumentModel instance to 
     * calling client.
     * @param parameter
     * @return 
     */
    @Override
    public DocumentModel store(DocumentUploadParameter parameter)
    {
        String originalFilename = parameter.getFile().getOriginalFilename();        
        originalFilename = Utilities.sanitizeRepositoryName(originalFilename.toLowerCase());        
        
        // create hash from file bytes       
        String storageHash = DigestUtils.md5Hex(originalFilename);    
        storageHash = UUID.randomUUID().toString(); // TEMPORARY!
        String path = Hashing.createDirectoryPathString(storageHash, 2);
        
        // hash bytes ans store 
        byte[] bytes;
        try
        {
            bytes = Utilities.toByteArray(parameter.getFile().getInputStream());
        } catch (IOException ex) {
            throw new DocumentStorageException("error reading bytes from document: " +parameter.getFile().getOriginalFilename());
        }
        String hash = DigestUtils.sha256Hex(bytes);        
        if(parameter.getIntegrity().equals(hash) == false){
            throw new DocumentStorageException("file content is corrupted or tampered with: " + parameter.getFile().getName());
        }
        
        // create new file document
        FileDocument document = new FileDocument();
        document.setFilename(originalFilename);
        document.setChecksum(parameter.getIntegrity());
        document.setPath(path);
        document.setStoredName(DigestUtils.md5Hex(UUID.randomUUID().toString())); //stored filename
        document.setHash(hash);
        
        // store and serrve
        repository.save(document);                
        
        documentStorage.store(parameter.getFile(), path, document.getStoredName());           
        DocumentModel dm = new DocumentModel();
        dm.setId(document.getId());
        return dm;                 
    }

    /**
     * Fetches FileDocument document from underlying data store with given document id 
     * and returns the bytes of stored document file in file system.
     * @param id
     * @return
     * @throws IOException 
     */
    @Override
    public byte[] download(String id) throws IOException
    {
        Optional<Archive> optional = archiveRepository.findById(id);
        Optional<FileDocument> optionalDocument = repository.findById(id);
        if(optionalDocument.isPresent() == false){
            throw new DocumentIdNotFoundException("document id not found: " + id);
        }         
        
        // get document
        FileDocument document = optionalDocument.get();
        StringBuilder sb = new StringBuilder(document.getPath());
        sb.append(File.separator);
        Path fullPath = Paths.get(sb.toString());
                
        File file = documentStorage.getFile(fullPath.toString(), document.getStoredName());
        FileInputStream fi = new FileInputStream(file);
                
        // check storage checksum
        String storageHash = document.getHash();
        byte[] bytes = null;
        try
        {        
            bytes = Utilities.toBytes(fi);
            String checksum = DigestUtils.sha256Hex(bytes);
            if (checksum.equals(storageHash) == false) {
                throw new BadChecksumException("file content is corrupted or tampered with: " + file.getName());
            }
        } catch (IOException ex) {
            throw new DocumentStorageException("error reading bytes from document: " + optional.get().getFilename());
        }
        
        return bytes;
    }
    
    
    
    
}
