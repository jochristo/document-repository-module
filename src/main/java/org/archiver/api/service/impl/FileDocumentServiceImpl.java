package org.archiver.api.service.impl;

import java.util.List;
import org.archiver.api.domain.FileDocument;
import org.archiver.api.repository.FileDocumentRepository;
import org.archiver.api.service.IFileDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileDocumentServiceImpl implements IFileDocumentService
{
    @Autowired private FileDocumentRepository repository;

    @Override
    public List<FileDocument> findActive() {
        return repository.findActive();
    }

    @Override
    public List<FileDocument> findInActive() {
        return repository.findInActive();
    }

    @Override
    public List<FileDocument> findActive(String id) {
        return repository.findActive(id);
    }

    @Override
    public List<FileDocument> findActiveByFilename(String filename) {
        return repository.findActiveByFilename(filename);
    }
    
}
