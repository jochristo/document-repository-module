package org.archiver.api.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.archiver.api.configuration.ArchiveStorageProperties;
import org.archiver.api.exception.DocumentStorageException;
import org.archiver.api.service.IDocumentStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Provides document handling operations.
 * @author ic
 */
@Component
public final class DocumentStorageImpl implements IDocumentStorage
{
    private final Path location;

    /**
     * Constructs DocumentStorageImpl instance with injected ArchiveStorageProperties instance.
     * @param properties 
     */
    @Autowired
    public DocumentStorageImpl(ArchiveStorageProperties properties) {
        this.location = Paths.get(properties.getLocation());   
        initialize();
    }     

    /**
     * Initialized the document storage handler: creates storage location folder.
     */
    @Override
    public void initialize()
    {
        try 
        {
            Files.createDirectories(location);
        }
        catch (IOException e) {
            throw new DocumentStorageException("Could not initialize storage path");
        }
    }

    /**
     * Loads the Path object from given storage path and filename.
     * @param storagePath
     * @param filename
     * @return 
     */
    @Override
    public Path load(String storagePath, String filename) {
        Path path = Paths.get(location.toString(), storagePath);                
        return path.resolve(filename);
    }

    /**
     * Gets the Resource object from given storage path and filename.
     * @param storagePath
     * @param filename
     * @return 
     */
    @Override
    public Resource get(String storagePath, String filename)
    {
        try
        {
            Path file = load(storagePath, filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new DocumentStorageException("Could not read file: " + filename);

            }
        }
        catch (MalformedURLException ex) {
            throw new DocumentStorageException("Could not read file: " + filename);
        }
    }

    /**
     * Stores in file system given multi-part file under provided storage path and with given storage filename.
     * @param file
     * @param storagePath
     * @param storedFilename 
     */
    @Override
    public void store(MultipartFile file, String storagePath, String storedFilename)
    {
        try
        {
            StringBuilder sb = new StringBuilder(storagePath);
            sb.append(File.separator);
            Path fullPath = Paths.get(location.toString(), sb.toString());
            Files.createDirectories(fullPath);
            Files.copy(file.getInputStream(), fullPath.resolve(storedFilename), StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException ex)
        {
            throw new DocumentStorageException("document was not saved: " + file.getOriginalFilename());
        }       
    }
    
    /**
     * Gets the File object from given storage path and filename.
     * @param storagePath
     * @param filename
     * @return
     * @throws IOException 
     */
    @Override
    public File getFile(String storagePath, String filename) throws IOException
    {
        try
        {
            Path file = load(storagePath, filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource.getFile();
            }
            else {
                throw new DocumentStorageException("Could not read file: " + filename);

            }
        }
        catch (MalformedURLException ex) {
            throw new DocumentStorageException("Could not read file: " + filename);
        }
    }    
    
}
