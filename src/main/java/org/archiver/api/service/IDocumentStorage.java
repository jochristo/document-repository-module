package org.archiver.api.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Defines document storage handing methods.
 * @author ic
 */
public interface IDocumentStorage
{
    void initialize();
    
    public Path load(String storagePath, String filename);           
        
    public Resource get(String storagePath, String filename);
    
    public void store(MultipartFile file, String storagePath, String storedFilename);
    
    public File getFile(String storagePath, String filename) throws IOException;
}
