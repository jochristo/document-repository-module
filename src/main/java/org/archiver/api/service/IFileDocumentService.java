package org.archiver.api.service;

import java.util.List;
import org.archiver.api.domain.FileDocument;

/**
 *
 * @author ic
 */
public interface IFileDocumentService
{
    
    public List<FileDocument> findActive();    
    
    public List<FileDocument> findInActive();        
    
    public List<FileDocument> findActive(String id);        
    
    public List<FileDocument> findActiveByFilename(String filename);     
    
}
