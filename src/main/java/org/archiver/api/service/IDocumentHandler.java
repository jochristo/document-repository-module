package org.archiver.api.service;

import java.io.IOException;
import java.util.List;
import org.archiver.api.domain.Archive;
import org.archiver.api.domain.FileDocument;
import org.archiver.api.model.DocumentModel;
import org.archiver.api.model.bind.DocumentUploadParameter;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * Defines methods to handle FileDocument operations.
 * @author ic
 */
public interface IDocumentHandler
{
    public DocumentModel create(DocumentUploadParameter parameter);
    
    public Archive getArchive(String id);
            
    public Resource getFile(String id);
     
    public DocumentModel createDocument(DocumentUploadParameter parameter);
    
    public FileDocument getDocument(String id);
    
    public Resource getDocumentFile(String id);
    
    public List<FileDocument> getAllDocuments();
    
    @Deprecated
    public List<FileDocument> getAllDocuments(PageRequest pageRequest);
    
    public List<FileDocument> getAllDocuments(Pageable pageable);
    
    public DocumentModel store(DocumentUploadParameter parameter);
    
    public byte[] download(String id) throws IOException;
    
}
