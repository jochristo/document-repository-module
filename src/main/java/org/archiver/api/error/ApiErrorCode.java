package org.archiver.api.error;

import org.springframework.http.HttpStatus;

/**
 * Defines specific API error codes and their description.
 * @author ic
 */
public enum ApiErrorCode
{
    
    // GENERAL API ERROR CODES
    
    RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "RESOURCE_NOT_FOUND"),
    
    MISSING_REQUEST_PARAMETER(HttpStatus.BAD_REQUEST, "2001", "MISSING_REQUEST_PARAMETER"),   
          
    INVALID_FORMAT(HttpStatus.BAD_REQUEST, "400", "INVALID_FORMAT"),
    
    SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE, "503", "SERVICE_UNAVAILABLE"),
    
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "400", "BAD_REQUEST"),
          
    
    // application-specific errors
    BAD_FILE_CHECKSUM(HttpStatus.BAD_REQUEST, "400", "BAD_FILE_CHECKSUM"),
    
    FILE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "500", "FILE_ERROR"),
    
    DOCUMENT_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "DOCUMENT_ID_NOT_FOUND"),  
    
    DOCUMENT_NOT_AVAILABLE(HttpStatus.NOT_FOUND, "404", "DOCUMENT_NOT_AVAILABLE"),
    ;    
    
    private final HttpStatus httpStatus;
    private final String appCode;
    private final String title;    
    
    private final int statusCode;
    public int getStatusCode()
    {
        return this.httpStatus.value();
    }
    
    public String getStatusAsString()
    {
        return String.valueOf(this.httpStatus.value());
    }    

    private ApiErrorCode(HttpStatus httpStatus, String appCode, String title)
    {
        this.httpStatus = httpStatus;
        this.appCode = appCode;
        this.title = title;
        this.statusCode = httpStatus.value();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getTitle() {
        return title;
    }    
    
}
