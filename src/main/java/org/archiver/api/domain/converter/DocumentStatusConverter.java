package org.archiver.api.domain.converter;

import org.archiver.api.domain.enumeration.DocumentStatus;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author ic
 */
public class DocumentStatusConverter implements Converter<DocumentStatus, String>{

    @Override
    public String convert(DocumentStatus s) {
        return s.getDescription();
    }
    
}
