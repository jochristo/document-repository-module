package org.archiver.api.domain.enumeration;

/**
 *
 * @author ic
 */
public enum DocumentStatus
{
    INACTIVE(0,"INACTIVE"),    
    
    ACTIVE(1,"ACTIVE")        
    ;
    
    private DocumentStatus(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }     
}
