package org.archiver.api.domain.enumeration;

/**
 *
 * @author ic
 */
public enum DocumentSortField
{
    ID(0,"id"),    
    
    FILENAME(1,"filename"),
    
    CREATED_AT(2,"createdAt"),
    
    STATUS(3,"status"),
    ;
    
    private DocumentSortField(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }     
    
   
}
