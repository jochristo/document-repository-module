package org.archiver.api.domain;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document(collection = "archiveDetails")
public class ArchiveDetails
{
    @Id
    private String id;
    
    @Field
    @NotNull
    private String storedName;
    
    @Field
    @NotNull
    private String path;
    
    @Field
    @NotNull
    private String storageHash;
    
    @DBRef(lazy = true, db = "db_documents")
    @NotNull
    private Archive archive;
    
    @Field
    @NotNull
    private Date createdAt;
    
    @Field
    private Date updatedAt;

    public ArchiveDetails() {
        this.createdAt = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepository() {
        return storedName;
    }

    public void setRepository(String repository) {
        this.storedName = repository;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStorageHash() {
        return storageHash;
    }

    public void setStorageHash(String storageHash) {
        this.storageHash = storageHash;
    }
    
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Archive getArchive() {
        return archive;
    }

    public void setArchive(Archive archive) {
        this.archive = archive;
    }
    
    
    
    
}