package org.archiver.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.archiver.api.domain.enumeration.DocumentStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "document")
@CompoundIndexes({@CompoundIndex(name = "id_filename_unique_index",unique = true,def ="{'_id' : 1, 'filename': 1}")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileDocument
{
    @Id    
    private String id;
    
    @Field    
    @NotNull
    private String filename;
    
    @Field
    @JsonProperty("integrity")
    @NotNull
    private String checksum;  
    
    @Field
    @NotNull
    @JsonIgnore
    private String storedName;
    
    @Field
    @NotNull
    @JsonIgnore
    private String path;
    
    @Field
    @NotNull
    @JsonIgnore
    private String hash;    
    
    @Field
    @NotNull
    @JsonIgnore
    private final Date createdAt;
    
    @Field
    @JsonIgnore
    private Date updatedAt;
    
    @Field
    @JsonIgnore
    private DocumentStatus status;   

    public FileDocument()
    {
        this.createdAt = new Date();
        this.status = DocumentStatus.ACTIVE;      
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getStoredName() {
        return storedName;
    }

    public void setStoredName(String storedName) {
        this.storedName = storedName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
        
    public Date getCreatedAt() {
        return createdAt;
    }

    public DocumentStatus getStatus() {
        return status;
    }

    public void setStatus(DocumentStatus status) {
        this.status = status;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.checksum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileDocument other = (FileDocument) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.filename, other.filename)) {
            return false;
        }
        return Objects.equals(this.storedName, other.storedName) 
            && Objects.equals(this.id, other.id) 
            && Objects.equals(this.filename, other.filename);
    }         

}
