package org.archiver.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document(collection = "archive")
@CompoundIndexes({@CompoundIndex(name = "_id_archiveId_unique_index",unique = true,def ="{'_id' : 1, 'filename': 1}")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Archive
{
    @Id    
    private String id;
    
    @Field    
    @NotNull
    private String filename;
    
    @Field
    @JsonProperty("integrity")
    @NotNull
    private String integrityHash;  
    
    @Field
    @NotNull
    private final Date createdAt;
    
    @Field
    private Date updatedAt;
    
    @Field
    private final int status;

    public Archive()
    {        
        this.createdAt = new Date();
        this.status = 1;        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getIntegrityHash() {
        return integrityHash;
    }

    public void setIntegrityHash(String integrityHash) {
        this.integrityHash = integrityHash;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
        
    
    
}
