package org.archiver.api.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 *
 * @author ic
 */
@Profile("prod")
@Component
@EnableConfigurationProperties
@ConfigurationProperties("document.storage")
public class BaseDocumentStorageConfiguration
{
    private String location;
  
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    
}
