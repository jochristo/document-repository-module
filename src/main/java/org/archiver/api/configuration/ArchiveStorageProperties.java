package org.archiver.api.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * @author ic
 */
@ConfigurationProperties(value = "storage")
public class ArchiveStorageProperties
{
    /**
     * Folder location for storing uploaded documents
     */
    @Value("${document.storage.location}")    
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }    
    
}
