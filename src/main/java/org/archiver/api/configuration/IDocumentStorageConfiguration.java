package org.archiver.api.configuration;

/**
 *
 * @author ic
 */
public interface IDocumentStorageConfiguration
{
    public String location();
}
