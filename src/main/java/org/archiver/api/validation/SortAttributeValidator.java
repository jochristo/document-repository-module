package org.archiver.api.validation;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.archiver.api.annotation.SortAttribute;
import org.archiver.api.domain.enumeration.DocumentSortField;

/**
 *
 * @author ic
 */
public class SortAttributeValidator implements ConstraintValidator<SortAttribute, String>
{
    protected String[] values = null;    

    @Override
    public void initialize(SortAttribute a)
    {
        values = new String[4];
        List<DocumentSortField> fields = Lists.newArrayList(DocumentSortField.values());
        for (int i = 0; i < fields.size(); i++) {
            values[i] = fields.get(i).getDescription();
        }        
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        
        if(t == null) return true;
        
        List<DocumentSortField> fields = Lists.newArrayList(DocumentSortField.values());        
        List<String> strings = new ArrayList();
        fields.forEach((item)->{strings.add(item.getDescription());});           
        
        String[] split = t.split(",");
        if(split.length > 0)
        {
            boolean exists = true;
            for(String s : split)
            {
                String trimmed = s.trim();
                if(strings.contains(trimmed) == false){
                    exists = false;
                }
            }
            return exists;
        }
        
        return strings.contains(t);
    }
    
}
