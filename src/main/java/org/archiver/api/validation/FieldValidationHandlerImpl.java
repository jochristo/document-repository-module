package org.archiver.api.validation;

import java.util.ArrayList;
import java.util.List;
import org.archiver.api.exception.FieldConstraintValidationException;
import org.archiver.api.exception.FieldValidationException;
import org.archiver.api.model.ErrorResponse;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

/**
 *
 * @author ic
 */
@Component
public class FieldValidationHandlerImpl implements IFieldValidationHandler
{
    private List<FieldError> objectErrors;
    private List<ErrorResponse> errorResponses;
    
    @Override
    public void resolveErrors(final List<FieldError> objectErrors)
    {
        this.objectErrors = objectErrors;
        this.errorResponses = new ArrayList();
        
        objectErrors.stream().forEachOrdered((item)->{
            ErrorResponse er = new ErrorResponse();
            er.setParameter(item.getField());
            er.setError(item.getDefaultMessage() + ": " + item.getRejectedValue()); // get rejected value
            this.errorResponses.add(er);
        });
        
        if(errorResponses.isEmpty() == false)
        {

            StringBuilder sb = new StringBuilder();
            String message = "Field errors occured: ";
            sb.append(message);
            errorResponses.stream().map((er) -> {                
                sb.append("[");
                sb.append(er.getError());
                return er;
            }).forEachOrdered((_item) -> {
                sb.append("]");
            });                      
            throw new FieldConstraintValidationException(errorResponses, sb.toString());
        }
    }

    @Override
    public void resolveErrors(FieldError fieldError) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void resolveErrors(final List<FieldError> fieldErrors, final Object target)
    {
        // read object's custom annotations: VerifisCode, VerifisTransferPin, etc.
        
        this.objectErrors = objectErrors;
        this.errorResponses = new ArrayList();
        
        objectErrors.stream().forEachOrdered((item)->{
            ErrorResponse er = new ErrorResponse();
            er.setParameter(item.getField());
            er.setError(item.getDefaultMessage() + ": " + item.getRejectedValue()); // get rejected value);
            this.errorResponses.add(er);
        });
        
        if(errorResponses.isEmpty() == false){
            throw new FieldValidationException(errorResponses.get(0)); // get first validation error
        }
    }
    
}
