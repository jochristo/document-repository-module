package org.archiver.api;

import org.springframework.http.MediaType;

/**
 * Defines API constants.
 * @author ic
 */
public interface Constants
{
    public final static String APPLICATION_JSON_UTF8_VALUE = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8";    
    
    public final static MediaType APPLICATION_JSON_UTF8 = MediaType.valueOf(APPLICATION_JSON_UTF8_VALUE);     
    
    public final static String APPLICATION_OCTET_STREAM_UTF8_VALUE = MediaType.APPLICATION_OCTET_STREAM_VALUE + ";charset=UTF-8"; 
    
    
    public interface RequiredFieldMessage
    {
        public final static String DOCUMENT_ID_REQUIRED = "document id is required";
        
        public final static String INTEGRITY_CHECKSUM_REQUIRED = "integrity is required";
        
        public final static String FILE_REQUIRED = "file is required";

    }   
    
}
