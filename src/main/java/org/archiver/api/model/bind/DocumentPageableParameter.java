package org.archiver.api.model.bind;

import javax.validation.constraints.Min;
import org.archiver.api.annotation.SortAttribute;

/**
 *
 * @author ic
 */
public class DocumentPageableParameter
{
    @Min(value = 0, message = "offset must be equal to or greater than zero")
    private int offset;
    
    @Min(value = 0, message = "limit must be equal to or greater than zero")
    private Integer limit;    
    
    @SortAttribute
    private String sortBy;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
    
    
    
}
