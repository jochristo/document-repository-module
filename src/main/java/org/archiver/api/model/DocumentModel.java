package org.archiver.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentModel
{
    
    private String id;  
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
}
