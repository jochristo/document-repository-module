package org.archiver.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.archiver.api.Constants;
import org.archiver.api.EndPoints;
import org.archiver.api.domain.Archive;
import org.archiver.api.domain.FileDocument;
import org.archiver.api.validation.IFieldValidationHandler;
import org.archiver.api.model.DocumentModel;
import org.archiver.api.model.bind.DocumentPageableParameter;
import org.archiver.api.model.bind.DocumentUploadParameter;
import org.archiver.api.repository.paging.OffsetPageable;
import org.archiver.api.service.IDocumentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * Rest controller for document service API calls.
 * @author ic
 */
@RestController
public class DocumentController
{
    private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);   
    
    @Autowired private IDocumentHandler documentHandler;
    @Autowired private IFieldValidationHandler fieldValidator;
       
    //@RequestMapping(value = EndPoints.DOCUMENTS_BY_ID, method=RequestMethod.GET, produces = Constants.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Deprecated
    public Archive get(@PathVariable String id)
    {
        Archive archive = documentHandler.getArchive(id);        
        return ResponseEntity.ok(archive).getBody();
    }
    
    @RequestMapping(value = EndPoints.DOCUMENTS, method=RequestMethod.POST, produces = Constants.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DocumentModel upload(@Valid @ModelAttribute DocumentUploadParameter parameter, BindingResult result)
    {                
        if (result.hasErrors()) {
            fieldValidator.resolveErrors(result.getFieldErrors());
        }    
        DocumentModel dm = documentHandler.store(parameter);
        return ResponseEntity.ok(dm).getBody();
    }    
    
    @RequestMapping(value = EndPoints.DOCUMENTS_BY_ID, method=RequestMethod.GET, produces = Constants.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public FileDocument getDocument(@PathVariable String id)
    {
        FileDocument document = documentHandler.getDocument(id);        
        return ResponseEntity.ok(document).getBody();
    }    

    @RequestMapping(value = EndPoints.DOCUMENTS_DOWNLOAD_BY_ID, method=RequestMethod.GET, produces = Constants.APPLICATION_OCTET_STREAM_UTF8_VALUE)
    public ResponseEntity<byte[]> download(@PathVariable String id) throws IOException
    {                
        //Resource resource = documentHandler.getDocumentFile(id);
        byte[] resource = documentHandler.download(id);
        FileDocument document = documentHandler.getDocument(id);                
        String servedFilename = document.getFilename();
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + servedFilename + "\"").body(resource);        
    }    
    
    @RequestMapping(value = EndPoints.DOCUMENTS, method=RequestMethod.GET, produces = Constants.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<FileDocument> getAllDocuments(@ModelAttribute @Valid DocumentPageableParameter parameter, BindingResult result)
    {
        List<FileDocument> documents = new ArrayList<>();
        // field validation
        if(parameter.getSortBy() != null){
            if(result.hasErrors())
            {            
                fieldValidator.resolveErrors(result.getFieldErrors());
            }             
            
            OffsetPageable offsetPageable = new OffsetPageable(parameter.getLimit(), parameter.getOffset(), new Sort(Sort.Direction.ASC, parameter.getSortBy()));
            documents = documentHandler.getAllDocuments(offsetPageable);
        }
        else
        {
            documents = documentHandler.getAllDocuments();
        }
        return ResponseEntity.ok(documents).getBody();
    }       
    
}
