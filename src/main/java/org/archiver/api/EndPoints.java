package org.archiver.api;

/**
 * Defines Verifis API endpoints.
 * @author ic
 */
public interface EndPoints
{
    public static final String CONTEXT_PATH = "";
    
    public static final String API_PATH = CONTEXT_PATH + "/rest/v1";
    
    public static final String SPRING_BOOT_ERRORS = "/error";	          
    
    public static final String API_ERROR_CODES = "/internal/codes";	    

    public static final String DOCUMENTS = API_PATH + "/documents"; //[POST]
       
    public static final String DOCUMENTS_BY_ID = API_PATH + "/documents/{id}"; //[GET]    
        
    public static final String DOCUMENTS_DOWNLOAD_BY_ID = API_PATH + "/documents/{id}/download"; //[GET]      
   
    
}
