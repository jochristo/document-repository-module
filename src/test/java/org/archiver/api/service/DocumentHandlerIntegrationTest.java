package org.archiver.api.service;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.codec.digest.DigestUtils;
import org.archiver.api.domain.FileDocument;
import org.archiver.api.model.DocumentModel;
import org.archiver.api.model.bind.DocumentUploadParameter;
import org.archiver.api.service.impl.DocumentHandlerImpl;
import org.archiver.tools.Utilities;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileSystemUtils;

/**
 *
 * @author ic
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DocumentHandlerIntegrationTest
{    
    @Autowired private DocumentHandlerImpl handler;
    @Autowired private IFileDocumentService service;
    @Autowired MongoOperations mongoOperations;    
    @Autowired MongoTemplate mongoTemplate;
    @Autowired private ResourceLoader resourceLoader;    
    private static final Logger logger = LoggerFactory.getLogger(DocumentHandlerIntegrationTest.class);
    private final String integrity = "4B0E90F3C1C60BD9E20FDDEBE6A5DEAF";
    private MockMultipartFile file = new MockMultipartFile("multipart.file", "multipart.file", "application/octet-stream", "multipart.file".getBytes());
    private DocumentModel dm = null; 
           
    @After
    public void after() throws IOException
    {        
        logger.info("Running @After public void after() method...");
        logger.info("Cleaning temp folder and test database...");
        // clean up temp folder and mongo test database
        Resource resource = resourceLoader.getResource("file:junit");        
        FileSystemUtils.deleteRecursively(resource.getFile().toPath());
        mongoTemplate.getDb().drop();
        logger.info("Done.");
    }
    
    //@Before
    public void before()
    {
        logger.info("Running @Before public void before() method...");
        initializeDummyData();
        /*
        DocumentUploadParameter parameter = new DocumentUploadParameter();
        parameter.setIntegrity(integrity);
        String filename = Utilities.randomFilename("ab");
        file = new MockMultipartFile(filename, filename, "application/octet-stream", filename.getBytes());
        parameter.setFile(file);
        dm = handler.store(parameter);       
        */
    }
    
    private void initializeDummyData()
    {        
        int num = 10;        
        try
        {
            logger.info("Initializing dummy file data..."); 
            for(int i = 0; i < num; i++)
            {                       
                int counter = i+1;
                logger.info("Writing file #" + counter);
                DocumentUploadParameter parameter = new DocumentUploadParameter();
                parameter.setIntegrity(integrity);
                String filename = Utilities.randomFilename("ab");
                file = new MockMultipartFile(filename, filename, "application/octet-stream", filename.getBytes());
                parameter.setFile(file);            
                DocumentModel documentModel = handler.store(parameter);
                FileDocument document = handler.getDocument(documentModel.getId());
                Resource resource = resourceLoader.getResource("file:junit"); 
                logger.info("Writing file " + filename + " to path: " + resource.getFile().toPath() + document.getPath() + document.getStoredName());
                assertNotNull(documentModel);
                logger.info("Done.");            
            }
        }
        catch (Exception ex)
        {            
            
        }                    
    }
    
    @Test
    public void testStorePasses() throws IOException
    {
        int num = 2000;
        logger.info("Running testStorePasses(() method...");
        try{
        for(int i = 0; i < num; i++)
        {   
            int counter = i+1;
            logger.info("Writing file #" + counter);
            DocumentUploadParameter parameter = new DocumentUploadParameter();
            parameter.setIntegrity(integrity);
            String filename = Utilities.randomFilename("ab");
            file = new MockMultipartFile(filename, filename, "application/octet-stream", filename.getBytes());
            parameter.setFile(file);            
            DocumentModel documentModel = handler.store(parameter);
            FileDocument document = handler.getDocument(documentModel.getId());
            Resource resource = resourceLoader.getResource("file:junit"); 
            logger.info("Writing file " + filename + " to path: " + resource.getFile().toPath() + document.getPath() + document.getStoredName());
            assertNotNull(documentModel);
            logger.info("Done.");            
        }        
        }
        catch (Exception ex)
        {            
            
        }
    }
     
    //@Test
    public void testDownloadPasses() throws IOException, InterruptedException
    {
        before();
        logger.info("Running testDownloadPasses() method...");
        List<FileDocument> documents = service.findActive();
        if(documents.isEmpty() == false)
        {
            for(int i = 0; i < 1; i++)
            {
                try
                {
                    FileDocument d = documents.get(i);
                    byte[] bytes = handler.download(d.getId());
                    assertNotNull(bytes);
                    assertEquals(d.getHash(), DigestUtils.sha256Hex(bytes));
                } 
                catch (IOException ex) {
                    java.util.logging.Logger.getLogger(DocumentHandlerIntegrationTest.class.getName()).log(Level.SEVERE, null, ex);
                }                
            }

        }

    }

}
