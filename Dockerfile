#use jdk
FROM openjdk:8-jdk-alpine

#define arguments
ARG mongo_container_name
ARG storage_path
ARG consul_name
ENV MONGO_NAME=$mongo_container_name
ENV DOCUMENTS_STORAGE_PATH=$storage_path
ENV CONSUL_NAME=$consul_name

# create folder path
RUN mkdir -p /opt/apps/docker/document-module
ENV PROJECT_HOME /opt/apps/docker/document-module
COPY target/document-repository-module-1.0.1.jar $PROJECT_HOME/document-repository-module-1.0.1.jar
#WORKDIR $PROJECT_HOME

# create mondo data folder :/opt/apps/docker/document-module/data/db
RUN mkdir -p /opt/apps/docker/document-module/data/db

#create app's document file foler: /opt/apps/docker/document-module/data/documents
RUN mkdir -p /opt/apps/docker/document-module/data/documents

#expose app's port to docker
EXPOSE 13333

# volume to mount container's documents directory
VOLUME /data/documents-volume


ENV UPLOAD_PATH=/opt/apps/docker/document-module/data/documents

WORKDIR $PROJECT_HOME
#CMD java -Dspring.data.mongodb.uri=mongodb://${MONGO_NAME}/db_documents -Ddocument.storage.location=$DOCUMENTS_STORAGE_PATH -jar /opt/apps/docker/document-module/document-repository-module-1.0.1.jar
CMD java -Dspring.data.mongodb.uri=mongodb://${MONGO_NAME}/db_documents -Ddocument.storage.location=/data/documents-volume -jar /opt/apps/docker/document-module/document-repository-module-1.0.1.jar

#CMD java -Dspring.cloud.consul.host=${CONSUL_NAME} -Dspring.data.mongodb.uri=mongodb://${MONGO_NAME}/db_documents -Ddocument.storage.location=/data/documents-volume -jar /opt/apps/docker/document-module/document-repository-module-1.0.1.jar
#CMD java -Ddocument.storage.location=/data/documents-volume -jar /opt/apps/docker/document-module/document-repository-module-1.0.1.jar